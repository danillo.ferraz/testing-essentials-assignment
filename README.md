#AC P2P - Engineering Essentials
##Week 2 Test Automation Essentials
### Assignment

This is a simple app for auctions:<br/>

User can: <br/> 
 - Create an auction, 
 - Search for all auctions
 - Search for a specific auction by its id
 - Place a Bid (TEN_PERCENT, FIFTY_PERCENT or ONE_HUNDRED_PERCENT)
 - Delete an auction

 ###Tests were made using the Test Pyramid. (kind of)<br/>

 There are:</br>
 - 3 acceptance tests
 - 3 integration tests 
 - 20 unit tests


To run the project enter the following commands on your terminal:<br/>

```
startDatabase.sh
```

then:
```
./mvnw spring-boot:run 
```

API can be tested with the provided Postman file:
```
week2auctionapp.postman_collection.json
```
