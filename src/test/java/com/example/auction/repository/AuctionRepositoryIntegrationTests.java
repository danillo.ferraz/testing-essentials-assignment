package com.example.auction.repository;

import com.example.auction.model.Auction;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(SpringExtension.class)
@DataJpaTest
public class AuctionRepositoryIntegrationTests {

    @Autowired
    AuctionRepository auctionRepository;

    Auction validAuctionOne;
    Auction validAuctionTwo;

    @BeforeEach
    public void setup() {
        validAuctionOne = new Auction(
                1L,
                "Danillo",
                "Lot Name",
                "Lot Description",
                10L,
                10L,
                true
        );
        validAuctionTwo = new Auction(
                2L,
                "Foo",
                "Rare Bar",
                "Foo Bar",
                100L,
                100L,
                true
        );
    }

    @AfterEach
    public void tearDown() throws Exception {
        auctionRepository.deleteAll();
    }

    @Test
    public void shouldSaveAndFetchAuction() throws Exception {
        auctionRepository.save(validAuctionOne);
        Optional<Auction> retrieved = auctionRepository.findById(1L);
        assertEquals(true, retrieved.isPresent());
        Auction retrievedAuction = retrieved.get();
        assertEquals(validAuctionOne.getId(), retrievedAuction.getId());
    }

    @Test
    public void shouldSaveAndFetchAllAuctions() throws Exception {
        auctionRepository.save(validAuctionOne);
        auctionRepository.save(validAuctionTwo);
        List<Auction> retrieved = auctionRepository.findAll();
        assertEquals(2, retrieved.size());
    }

    @Test
    public void shouldSaveAndDeleteAuction() throws Exception {
        auctionRepository.save(validAuctionOne);
        auctionRepository.deleteAll();

        Optional<Auction> shouldBeEmpty = auctionRepository.findById(validAuctionOne.getId());
        assertEquals(false, shouldBeEmpty.isPresent());
    }


}
