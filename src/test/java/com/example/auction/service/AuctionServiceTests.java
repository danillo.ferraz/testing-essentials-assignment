package com.example.auction.service;

import com.example.auction.dto.AuctionDTO;
import com.example.auction.dto.BidDTO;
import com.example.auction.model.Auction;
import com.example.auction.repository.AuctionRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static java.util.Optional.ofNullable;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class AuctionServiceTests {

    @InjectMocks
    AuctionService service;

    @Mock
    AuctionRepository repository;

    Auction validAuctionOne;
    Auction validAuctionTwo;
    List<Auction> validAuctionList = new ArrayList<>();
    AuctionDTO validAuctionDTOOne;

    @BeforeEach
    void setup() {
        validAuctionOne = new Auction(
                1L,
                "Danillo",
                "Lot Name",
                "Lot Description",
                10L,
                10L,
                true
        );
        validAuctionTwo = new Auction(
                2L,
                "Foo",
                "Rare Bar",
                "Foo Bar",
                100L,
                100L,
                true
        );

        validAuctionDTOOne = new AuctionDTO(
                "Danillo",
                "Lot Name",
                "Lot Description",
                10L );

        validAuctionList.add(validAuctionOne);
        validAuctionList.add(validAuctionTwo);
    }

    @Test
    void findAllAuctions_shouldReturnAuctionList() {
        when(repository.findAll()).thenReturn(validAuctionList);

        List<Auction> received = service.findAllAuctions();

        assertEquals(validAuctionList, received);
    }

    @Test
    void getAuction_givenId_shouldReturnAuction() {
        when(repository.findById(1L)).thenReturn(ofNullable(validAuctionOne));

        Optional<Auction> received = service.getAuction(1L);
        Auction auction = received.get();
        assertEquals(validAuctionOne.getConsignorName(), auction.getConsignorName());
        assertEquals(validAuctionOne.getFinalPrice(), auction.getFinalPrice());
        assertEquals(validAuctionOne.getId(), auction.getId());
        assertEquals(validAuctionOne.getLotDescription(), auction.getLotDescription());
        assertEquals(validAuctionOne.getLotName(), auction.getLotName());
        assertEquals(validAuctionOne.isActive(), auction.isActive());
        assertEquals(validAuctionOne.getStartingPrice(), auction.getStartingPrice());
    }

    @Test
    void getAuction_givenInvalidId_shouldReturnEmptyOptional() {
        when(repository.findById(any())).thenReturn(Optional.empty());
        Optional<Auction> received = service.getAuction(1L);

        assertEquals(Optional.empty(), received);

    }

    @Test
    void createAuction_givenAuctionDTO_shouldReturnAuction(){
        when(repository.save(any(Auction.class))).thenReturn(validAuctionOne);

        Auction received = service.createAuction(validAuctionDTOOne);

        assertEquals(validAuctionOne.getConsignorName(), received.getConsignorName());
        assertEquals(validAuctionOne.getFinalPrice(), received.getFinalPrice());
        assertEquals(validAuctionOne.getId(), received.getId());
        assertEquals(validAuctionOne.getLotDescription(), received.getLotDescription());
        assertEquals(validAuctionOne.getLotName(), received.getLotName());
        assertEquals(validAuctionOne.isActive(), received.isActive());
        assertEquals(validAuctionOne.getStartingPrice(), received.getStartingPrice());

    }

    @Test
    void createAuction_givenAuctionDTO_auctionStartingAndFinalPriceMustBeEqual() {
        when(repository.save(any(Auction.class))).thenReturn(validAuctionOne);
        Auction received = service.createAuction(validAuctionDTOOne);
        assertEquals(received.getStartingPrice(), received.getFinalPrice());
    }

    @Test
    void createAuction_givenAuctionDTO_auctionStartingAndFinalPriceMustBeTrue() {
        when(repository.save(any(Auction.class))).thenReturn(validAuctionOne);
        Auction received = service.createAuction(validAuctionDTOOne);
        assertEquals(true, received.isActive());
    }

    @Test
    void bidIncrement_givenBidITO10percent_shouldReturnAuction() {
        double initialFinalPrice = 100;
        double expectedFinalPrice = 110;
        validAuctionOne.setFinalPrice(initialFinalPrice);
        when(repository.findById(1L)).thenReturn(ofNullable(validAuctionOne));
        when(repository.save(any(Auction.class))).thenReturn(validAuctionOne);

        service.bidIncrement(1, BidDTO.TEN_PERCENT);
        assertEquals(expectedFinalPrice, validAuctionOne.getFinalPrice());
    }

    @Test
    void bidIncrement_givenBidITO50percent_shouldReturnAuction() {
        double initialFinalPrice = 100;
        double expectedFinalPrice = 150;
        validAuctionOne.setFinalPrice(initialFinalPrice);
        when(repository.findById(1L)).thenReturn(ofNullable(validAuctionOne));
        when(repository.save(any(Auction.class))).thenReturn(validAuctionOne);

        service.bidIncrement(1, BidDTO.FIFTY_PERCENT);
        assertEquals(expectedFinalPrice, validAuctionOne.getFinalPrice());
    }

    @Test
    void bidIncrement_givenBidITO100percent_shouldReturnAuction() {
        double initialFinalPrice = 100;
        double expectedFinalPrice = 200;
        validAuctionOne.setFinalPrice(initialFinalPrice);
        when(repository.findById(1L)).thenReturn(ofNullable(validAuctionOne));
        when(repository.save(any(Auction.class))).thenReturn(validAuctionOne);

        service.bidIncrement(1, BidDTO.ONE_HUNDRED_PERCENT);
        assertEquals(expectedFinalPrice, validAuctionOne.getFinalPrice());
    }

    @Test
    void bidIncrement_givenInvalidId_shouldReturnNullableOptional() {
        when(repository.findById(1L)).thenReturn(Optional.ofNullable(null));
        Optional<Auction> updated = service.bidIncrement(1L, BidDTO.TEN_PERCENT);
        assertEquals(false, updated.isPresent());
    }

    @Test
    void deleteAuction_givenValidId_shouldReturnTrue() {
        when(repository.findById(1L))
                .thenReturn(Optional.ofNullable(validAuctionOne));

        boolean received = service.deleteAuction(1);
        assertEquals(true, received);

    }

    @Test
    void deleteAuction_givenInvalidId_shouldReturnFalse() {
        when(repository.findById(1L))
                .thenReturn(Optional.empty());

        boolean received = service.deleteAuction(1);
        assertEquals(false, received);
    }

}
