package com.example.auction.cucumber;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions( features = "classpath:features",
        glue = "com.example.auction.cucumber",
        plugin = { "pretty", "json:target/cucumber.json"},
        publish = true)
public class CucumberAcceptanceTests {
}
