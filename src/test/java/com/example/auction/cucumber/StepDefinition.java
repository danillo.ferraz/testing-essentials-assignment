package com.example.auction.cucumber;

import com.example.auction.dto.AuctionDTO;
import com.example.auction.model.Auction;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.cucumber.spring.CucumberContextConfiguration;
import org.assertj.core.api.Assertions;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import static org.junit.jupiter.api.Assertions.assertEquals;


@CucumberContextConfiguration
@SpringBootTest(webEnvironment= SpringBootTest.WebEnvironment.DEFINED_PORT)
public class StepDefinition {

    private static final String URL = "http://localhost:8080/auction";

    private RestTemplate restTemplate = new RestTemplate();

    Auction retreivedAuction;


    @When("user provides the values to create an Auction")
    public void userCreatesAuction() {
        AuctionDTO dto = new AuctionDTO(
                "Danillo",
                "Guitar",
                "Fender",
                1000
        );
        ResponseEntity<Auction> response = restTemplate.postForEntity(URL, dto, Auction.class);
        retreivedAuction = response.getBody();
        assertEquals(HttpStatus.CREATED, response.getStatusCode());

    }

    @Then("server should return the created Auction")
    public void userGetsCreatedAuction() {
        assertEquals("Danillo", retreivedAuction.getConsignorName());
        assertEquals("Guitar", retreivedAuction.getLotName());
        assertEquals("Fender", retreivedAuction.getLotDescription());
        assertEquals(1000.0, retreivedAuction.getStartingPrice());
        assertEquals(1000.0, retreivedAuction.getFinalPrice());
        assertEquals(true, retreivedAuction.isActive());
    }

    @When("user requests for an auction given an existing ID")
    public void userSearchesAuctionByID() {
        AuctionDTO dto = new AuctionDTO(
                "Danillo",
                "Guitar",
                "Fender",
                1000
        );
        restTemplate.postForEntity(URL, dto, Auction.class);
        ResponseEntity<Auction> response = restTemplate.getForEntity(URL +"/1", Auction.class);
        retreivedAuction = response.getBody();
        assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    @Then("requested Auction is returned")
    public void userGetsRequestedAuction () {
        assertEquals(1, retreivedAuction.getId());
        assertEquals("Danillo", retreivedAuction.getConsignorName());
        assertEquals("Guitar", retreivedAuction.getLotName());
        assertEquals("Fender", retreivedAuction.getLotDescription());
        assertEquals(1000.0, retreivedAuction.getStartingPrice());
        assertEquals(1000.0, retreivedAuction.getFinalPrice());
        assertEquals(true, retreivedAuction.isActive());
    }

    @When("user deletes an auction given an existing ID")
    public void userDeletesAuctionByID() {
        restTemplate.delete(URL + "/1");
    }

    @Then("the register should not exist anymore")
    public void auctionDoesNotExistsAnymore() {
        Assertions.assertThatThrownBy(() -> restTemplate.getForEntity(URL +"/1", Auction.class)).
                isInstanceOf(HttpClientErrorException.class);
    }


}
