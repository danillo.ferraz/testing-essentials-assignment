package com.example.auction.controller;

import com.example.auction.dto.AuctionDTO;
import com.example.auction.dto.BidDTO;
import com.example.auction.model.Auction;
import com.example.auction.service.AuctionService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static java.util.Optional.ofNullable;
import static org.hamcrest.Matchers.hasSize;
import static org.mockito.ArgumentMatchers.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;


import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@WebMvcTest(AuctionController.class)
class AuctionControllerTests {

    @Autowired
    MockMvc mockMvc;

    @MockBean
    private AuctionService auctionService;

    Auction validAuctionOne;
    Auction validAuctionTwo;
    AuctionDTO validAuctionDTOOne;
    List<Auction> validAuctionList;
    BidDTO tenPercentBid =  BidDTO.TEN_PERCENT;

    @BeforeEach
    void setup() {
        validAuctionOne = new Auction(
                1L,
                "Danillo",
                "Lot Name",
                "Lot Description",
                10L,
                10L,
                true
        );
        validAuctionTwo = new Auction(
                2L,
                "Foo",
                "Rare Bar",
                "Foo Bar",
                100L,
                100L,
                true
        );
        validAuctionDTOOne = new AuctionDTO(
                "Danillo",
                "Lot Name",
                "Lot Description",
                10L
        );

        validAuctionList = new ArrayList<Auction>();
        validAuctionList.add(validAuctionOne);
        validAuctionList.add(validAuctionTwo);
    }

    @Test
    void createAuction_givenValidInput_shouldReturnCreatedAuction() throws Exception {
        when(auctionService.createAuction(any(AuctionDTO.class)))
                .thenReturn(validAuctionOne);

        this.mockMvc.perform(
                post("/auction")
                        .content(asJsonString(validAuctionDTOOne))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.id").exists())
                .andExpect(jsonPath("$.consignorName").exists())
                .andExpect(jsonPath("$.lotName").exists())
                .andExpect(jsonPath("$.lotDescription").exists())
                .andExpect(jsonPath("$.startingPrice").exists())
                .andExpect(jsonPath("$.active").exists())
                .andDo(MockMvcResultHandlers.print());
    }

    public static String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Test
    void getAllAuctions_shouldReturnListOfAuctions() throws Exception {
        when(auctionService.findAllAuctions())
                .thenReturn(validAuctionList);

        this.mockMvc.perform(get("/auction"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.*", hasSize(2)));
    }

    @Test
    void getAuction_givenValidId_shouldReturnAuction() throws Exception {
        when(auctionService.getAuction(1))
                .thenReturn(ofNullable(validAuctionOne));

        this.mockMvc.perform(get("/auction/1"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("id").value(1L))
                .andExpect(jsonPath("consignorName").value("Danillo"))
                .andExpect(jsonPath("lotName").value("Lot Name"))
                .andExpect(jsonPath("lotDescription").value("Lot Description"))
                .andExpect(jsonPath("startingPrice").value(10L))
                .andExpect(jsonPath("finalPrice").value(10L))
                .andExpect(jsonPath("active").value(true));
    }

    @Test
    void getAuction_givenInvalidId_shouldReturnNotFound() throws Exception {
        when(auctionService.getAuction(1)).thenReturn(Optional.empty());
        this.mockMvc.perform(
                get("/auction/1"))
                .andExpect(status().isNotFound());
    }
    @Test
    void placeBid_givenValidId_shouldReturnUpdatedAuction() throws Exception {
        when(auctionService.getAuction(anyLong()))
                .thenReturn(ofNullable(validAuctionOne));
        when(auctionService.bidIncrement(validAuctionOne.getId(), tenPercentBid))
                .thenReturn(ofNullable(validAuctionOne));

        this.mockMvc.perform(
                patch("/auction/1/TEN_PERCENT"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").exists())
                .andExpect(jsonPath("$.consignorName").exists())
                .andExpect(jsonPath("$.lotName").exists())
                .andExpect(jsonPath("$.lotDescription").exists())
                .andExpect(jsonPath("$.startingPrice").exists())
                .andExpect(jsonPath("$.active").exists())
                .andDo(MockMvcResultHandlers.print());
    }

    @Test
    void placeBid_givenInvalidId_shouldReturnNotFound() throws Exception {
        when(auctionService.getAuction(anyLong()))
                .thenReturn(null);

        this.mockMvc.perform(
                patch("/auction/1/bid/INVALID_BID"))
                .andExpect(status().isNotFound());

    }

    @Test
    void deleteAuction_giveValidId_shouldDeleteAuction() throws Exception {
        when(auctionService.deleteAuction(1L))
                .thenReturn(true);

        this.mockMvc.perform(
                delete("/auction/1"))
                .andExpect(status().isNoContent());

    }

    @Test
    void deleteAuction_giveInvalidId_shouldReturnNotFound() throws Exception {
        when(auctionService.deleteAuction(1000L))
                .thenReturn(false);

        this.mockMvc.perform(
                delete("/auction/1000"))
                .andExpect(status().isNotFound());
    }

}
