Feature: Testing Auction Application
  Users should be able create an Auction and search for them by its ID

  Scenario: Create an Auction
    When user provides the values to create an Auction
    Then server should return the created Auction

  Scenario: Search for an Auction by its ID
    When user requests for an auction given an existing ID
    Then requested Auction is returned

  Scenario: Delete for an Auction by its ID
    When user deletes an auction given an existing ID
    Then the register should not exist anymore


