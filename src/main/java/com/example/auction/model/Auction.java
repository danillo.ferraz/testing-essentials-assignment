package com.example.auction.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Auction {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    private String consignorName; // the owner of the product
    private String lotName; // the item to be sold
    private String lotDescription;
    private double startingPrice;
    private double finalPrice;
    private boolean active;

    public Auction() {
    }

    public Auction(
            long id,
            String consignorName,
            String lotName,
            String lotDescription,
            double startingPrice,
            double finalPrice,
            boolean active) {
        this.id = id;
        this.consignorName = consignorName;
        this.lotName = lotName;
        this.lotDescription = lotDescription;
        this.startingPrice = startingPrice;
        this.finalPrice = finalPrice;
        this.active = active;
    }

    public Auction(
            String consignorName,
            String lotName,
            String lotDescription,
            double startingPrice,
            double finalPrice,
            boolean active
    ) {
        this.consignorName = consignorName;
        this.lotName = lotName;
        this.lotDescription = lotDescription;
        this.startingPrice = startingPrice;
        this.finalPrice = finalPrice;
        this.active = active;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getConsignorName() {
        return consignorName;
    }

    public void setConsignorName(String consignorName) {
        this.consignorName = consignorName;
    }

    public String getLotName() {
        return lotName;
    }

    public void setLotName(String lotName) {
        this.lotName = lotName;
    }

    public String getLotDescription() {
        return lotDescription;
    }

    public void setLotDescription(String lotDescription) {
        this.lotDescription = lotDescription;
    }

    public double getStartingPrice() {
        return startingPrice;
    }

    public void setStartingPrice(double startingPrice) {
        this.startingPrice = startingPrice;
    }

    public double getFinalPrice() {
        return finalPrice;
    }

    public void setFinalPrice(double finalPrice) {
        this.finalPrice = finalPrice;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }
}
