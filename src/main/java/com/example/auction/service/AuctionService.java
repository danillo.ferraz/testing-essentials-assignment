package com.example.auction.service;

import com.example.auction.dto.AuctionDTO;
import com.example.auction.dto.BidDTO;
import com.example.auction.model.Auction;
import com.example.auction.repository.AuctionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class AuctionService {

    private AuctionRepository auctionRepository;

    @Autowired
    public AuctionService(AuctionRepository auctionRepository) {
        this.auctionRepository = auctionRepository;
    }

    public Optional<Auction> getAuction(long id) {
        return auctionRepository.findById(id);

    }

    public Auction createAuction(AuctionDTO auctionDTO) {

        Auction auction = new Auction(
                auctionDTO.getConsignorName(),
                auctionDTO.getLotName(),
                auctionDTO.getLotDescription(),
                auctionDTO.getStartingPrice(),
                auctionDTO.getStartingPrice(),//startingPrice is initiated equals to final price
                true);

        return auctionRepository.save(auction);
    }

    public List<Auction> findAllAuctions() {
        return auctionRepository.findAll();
    }

    public Optional<Auction> bidIncrement(long id, BidDTO bidDTO) {
        Optional<Auction> auction = auctionRepository.findById(id)
                .map(a -> {
                    a.setFinalPrice(updateFinalPrice(a, bidDTO));
                    return auctionRepository.save(a);
                });
        return auction;
    }

    private double updateFinalPrice(Auction a, BidDTO bid) {
        double currentPrice = a.getFinalPrice();
        return currentPrice + (currentPrice * bid.getValue());
    }

    public Boolean deleteAuction(long id) {
        Optional<Auction> auction = auctionRepository.findById(id);
        if(auction.isPresent()){
            auctionRepository.deleteById(id);
            return true;
        }
        return false;
    }
}
