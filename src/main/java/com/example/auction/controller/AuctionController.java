package com.example.auction.controller;

import com.example.auction.dto.AuctionDTO;
import com.example.auction.dto.BidDTO;
import com.example.auction.model.Auction;
import com.example.auction.service.AuctionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
public class AuctionController {

    @Autowired
    AuctionService auctionService;

    @GetMapping("/auction/{id}")
    public ResponseEntity<Auction> getAuction(@PathVariable("id") long id) {
        Optional<Auction> auction = auctionService.getAuction(id);
        if (auction.isPresent()) {
            return ResponseEntity.status(HttpStatus.OK).body(auction.get());
        } else {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }

    }

    @GetMapping("/auction")
    public ResponseEntity<List<Auction>> getAllAuctions() {
        List<Auction> auctionList = auctionService.findAllAuctions();
        return ResponseEntity.status(HttpStatus.OK).body(auctionList);
    }

    @PostMapping("/auction")
    public ResponseEntity<Auction> createAuction(@RequestBody AuctionDTO auctionDTO) {
        Auction newAuction = auctionService.createAuction(auctionDTO);
        return ResponseEntity.status(HttpStatus.CREATED).body(newAuction);
    }

    @PatchMapping("/auction/{id}/{bid}")
    public ResponseEntity<Auction> placeBid(@PathVariable("id") long id, @PathVariable("bid") BidDTO bidDTO) {

        Optional<Auction> updatedAuction = auctionService.bidIncrement(id, bidDTO);
        if (updatedAuction.isPresent()) {
            return ResponseEntity.status(HttpStatus.OK).body(updatedAuction.get());
        } else {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
    }

    @DeleteMapping("/auction/{id}")
    public ResponseEntity<?> deleteAuction(@PathVariable("id") long id) {
        boolean isDeleted = auctionService.deleteAuction(id);
        if (isDeleted) {
            return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
        } else {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
    }
}
