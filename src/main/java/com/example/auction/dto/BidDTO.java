package com.example.auction.dto;

import com.fasterxml.jackson.annotation.JsonFormat;

@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public enum BidDTO {
    TEN_PERCENT(0.1),
    FIFTY_PERCENT(0.5),
    ONE_HUNDRED_PERCENT(1);

    private double value;

    BidDTO(double value) {
        this.value = value;
    }

    public double getValue(){
        return this.value;
    }
}
