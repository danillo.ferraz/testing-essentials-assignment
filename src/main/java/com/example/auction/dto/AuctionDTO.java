package com.example.auction.dto;

public class AuctionDTO {

    private String consignorName;
    private String lotName;
    private String lotDescription;
    private double startingPrice;

    public AuctionDTO() {
    }

    public AuctionDTO(
            String consignorName,
            String lotName,
            String lotDescription,
            double startingPrice) {
        this.consignorName = consignorName;
        this.lotName = lotName;
        this.lotDescription = lotDescription;
        this.startingPrice = startingPrice;
    }

    public String getConsignorName() {
        return consignorName;
    }

    public void setConsignorName(String consignorName) {
        this.consignorName = consignorName;
    }

    public String getLotName() {
        return lotName;
    }

    public void setLotName(String lotName) {
        this.lotName = lotName;
    }

    public String getLotDescription() {
        return lotDescription;
    }

    public void setLotDescription(String lotDescription) {
        this.lotDescription = lotDescription;
    }

    public double getStartingPrice() {
        return startingPrice;
    }

    public void setStartingPrice(double startingPrice) {
        this.startingPrice = startingPrice;
    }
}
